# Translatios for aCMSTS antiX Community Multilanguage Support Test Suite Ver 0.37a
# Copyright (C) 2021 THE aCMSTS'S COPYRIGHT HOLDER
# This file is distributed under the same license as the aCMSTS package.
# The antiX Community, 2021
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: aCMSTS 0.37a\n"
"Report-Msgid-Bugs-To: forum.antiXlinux.com\n"
"POT-Creation-Date: 2021-03-21 14:46+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: antiX-contribs (transifex.com)\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: antiX-multilanguage-test-suite.sh:58
msgid "antiX Community Multilanguage Test Suite"
msgstr ""

#: antiX-multilanguage-test-suite.sh:82
msgid ""
"\\nˮpoeditˮ is not installed.\\n   --> Please install ˮpoeditˮ before "
"executing this script.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:83
msgid ""
"\\nˮmsgunfmtˮ command is not found.\\n   --> Please install ˮgettextˮ before "
"executing this script.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:85
msgid ""
"\\n No compatible Terminal Emulator found.\\n   --> Please make sure either "
"ˮLXterminalˮ\\n\\tor ˮROXtermˮ is installed on your\\n\\tsystem before "
"executing this script.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:86
msgid ""
"\\nˮyadˮ is not installed.\\n   --> Please install ˮyadˮ before executing "
"this script.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:87
msgid ""
"\\nˮgksudoˮ is not installed.\\n   --> Please install ˮgksudoˮ before "
"executing this script.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:88
msgid ""
"\\nˮsedˮ is not installed.\\n   --> Please install ˮsedˮ before executing "
"this script.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:89
msgid ""
"\\nˮawkˮ is not installed.\\n   --> Please make sure either ˮawkˮ or ˮgawkˮ "
"is installed\\n\\ton your system before executing this script.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:90
msgid ""
"\\nˮbashˮ is not installed.\\n   --> Please make sure true ˮbashˮ is "
"available on your\\n\\tsystem before executing this script.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:91
msgid ""
"\\nˮwmctrlˮ is not installed.\\n   --> Please install ˮwmctrlˮ before "
"executing this script.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:92
msgid ""
"\\nˮgrepˮ is not installed.\\n   --> Please install ˮgrepˮ before executing "
"this script.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:93
msgid ""
"\\nˮpgrepˮ is not installed.\\n   --> Please install ˮpgrepˮ before "
"executing this script.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:148
msgid "$TXT_TITLE, Version $VERSION\\n\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:203
msgid "If option -e is set either option -p or -m must be set also."
msgstr ""

#: antiX-multilanguage-test-suite.sh:204
msgid "If either option -m or -p is set option -e must be set also."
msgstr ""

#: antiX-multilanguage-test-suite.sh:210 antiX-multilanguage-test-suite.sh:400
#: antiX-multilanguage-test-suite.sh:465 antiX-multilanguage-test-suite.sh:557
#: antiX-multilanguage-test-suite.sh:604 antiX-multilanguage-test-suite.sh:648
#: antiX-multilanguage-test-suite.sh:1009
#: antiX-multilanguage-test-suite.sh:1035
#: antiX-multilanguage-test-suite.sh:1122
#: antiX-multilanguage-test-suite.sh:1171
msgid "Abort"
msgstr ""

#: antiX-multilanguage-test-suite.sh:211
msgid "Back"
msgstr ""

#: antiX-multilanguage-test-suite.sh:214
msgid ""
"\\n\\tHelp not implemented yet\\n\\n\\tin short:\\n\\t-h\\thelp\\n\\t-e"
"\\texperienced user\\n\\t\\t(only in connection with either -m or -p)\\n\\t-m"
"\\tedit menu translations\\n\\t-p\\tedit program translations\\n\\t\\t(-m "
"and -p only in connection with -e).\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:215
msgid ""
"\\t-u 1\\tno window decorations\\n\\t-u 2\\tno icons\\n\\t-u 3\\tneither "
"window decorations nor icons\\n\\t-d\\tprint some debug information when "
"started\\n\\t\\tin terminal window\\n\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:218
msgid "OK"
msgstr ""

#: antiX-multilanguage-test-suite.sh:225
msgid ""
"\\n \\n<b>Help not implemented yet.</b>\\n\\nin short:\\n-h\\t\\thelp\\n-e\\t"
"\\texperienced user\\n\\t\\t(only in connection with either -m or -p)\\t\\n-m"
"\\t\\tedit menu translations\\n-p\\t\\tedit program translations\\n\\t\\t(-m "
"and -p only in connection with -e).\\t\\n-u\\t1\\tno window decorations.\\t"
"\\n-u\\t2\\tno icons\\t\\n-u\\t3\\tneither window decorations nor icons\\t"
"\\n-d\\t\\tprint some debug information when\\t\\n\\t\\tstarted in terminal "
"window\\t\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:246
msgid ""
"\\n<b>$TXT_TITLE (aCMSTS)\\nVersion $VERSION</b>\\n \\n\\tPlease chose which "
"kind of translation file you\\n\\twould like to access:\\n\\n\\tYou may "
"improve or amend translation strings\\n\\tin either existing <u><i>program</"
"i></u> translations\\n\\t(.mo files found in system) or <u><i>menue</i></u>"
"\\n\\ttranslations (.desktop files found in system).\\n\\tYou may also add a "
"new translation to any\\n\\tlanguage known to antiX.\\n\\n\\tPlease consider "
"kindly to transfer your improve-\\n\\tments to <u>transifex</u> after having "
"successfully\\n\\ttested them within testing environment provided\\n\\tby "
"this test suite. By this means you enable\\n\\tpeople using antiX worldwide "
"stand to benefit\\n\\tfrom your efforts.\\n\\n\\t<b>Please keep in mind your "
"improvements will\\n\\tstay permanent on your system (with regard\\n\\tto "
"updating antiX or the program itself) only\\n\\twhen applying them to "
"transifex.</b>\\n\\n\\tExperienced users may head for comand-line\\n"
"\\toptions, saving them from some extra clicks.\\n\\tBeyond this experienced "
"users may add and\\n\\ttest translations even to languages not\\n\\tknown by "
"antiX yet."
msgstr ""

#: antiX-multilanguage-test-suite.sh:248 antiX-multilanguage-test-suite.sh:745
#: antiX-multilanguage-test-suite.sh:1445
msgid "Exit"
msgstr ""

#: antiX-multilanguage-test-suite.sh:248
msgid "Menu"
msgstr ""

#: antiX-multilanguage-test-suite.sh:248
msgid "Program"
msgstr ""

#: antiX-multilanguage-test-suite.sh:248
msgid "Help"
msgstr ""

#: antiX-multilanguage-test-suite.sh:392
msgid "Add Application to Translations"
msgstr ""

#: antiX-multilanguage-test-suite.sh:394
msgid ""
"\\n\\tPlease press Button “Add New Application“\\n\\tin order to manually "
"create a <u>first translation</u>\\n\\tfor a program accordingly prepared. "
"See\\n\\thelp for details."
msgstr ""

#: antiX-multilanguage-test-suite.sh:400
msgid ""
"\\n<b>Please select an application from pulldown menu.</b>\\n\\tOnly "
"Programs with at least one existing\\n\\ttranslation to any language are "
"listed.$TXT_BODY_EXP\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:400
msgid "Available applications:"
msgstr ""

#: antiX-multilanguage-test-suite.sh:400
msgid "Proceed in selection"
msgstr ""

#: antiX-multilanguage-test-suite.sh:460
msgid ""
"\\n<b>Please select language of translation to edit\\nfrom pulldown menu.</b>"
msgstr ""

#: antiX-multilanguage-test-suite.sh:461
msgid "\\n\\n<u>Selected Application:</u>\\t“$SELECTED_APPLICATION”\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:462
msgid ""
"\\n\\tThe list contains only languages to which the\\n\\tprogram is "
"translated already. Please press\\n\\tButton “Add Translation“ in order to "
"create\\n\\ta new translation to a language <u>not in list</u>."
msgstr ""

#: antiX-multilanguage-test-suite.sh:465
msgid "Languages available:"
msgstr ""

#: antiX-multilanguage-test-suite.sh:465
msgid "Back to Application selection"
msgstr ""

#: antiX-multilanguage-test-suite.sh:465
msgid "Add new Translation"
msgstr ""

#: antiX-multilanguage-test-suite.sh:465 antiX-multilanguage-test-suite.sh:557
#: antiX-multilanguage-test-suite.sh:1120
msgid "Selection done"
msgstr ""

#: antiX-multilanguage-test-suite.sh:544
msgid ""
"\\n<b>Add new language</b>\\n\\tPlease select a language to add to the "
"existing\\n\\ttranslations of selected application from pulldown\\n\\tmenu "
"and press “Proceed” to create a new translation\\n\\tfor the tchosen "
"language."
msgstr ""

#: antiX-multilanguage-test-suite.sh:545
msgid "\\n\\n<u>Selected Application:</u>\\t“$SELECTED_APPLICATION”"
msgstr ""

#: antiX-multilanguage-test-suite.sh:546
msgid "\\n<u>Existing translations:</u>\\t$LANGLIST_01"
msgstr ""

#: antiX-multilanguage-test-suite.sh:547
msgid ""
"\\n\\n\\tThe pulldown menu contains all languages known to system\\n\\tby "
"now but omits any language to which the program\\n\\tis translated yet.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:548
msgid ""
"\\tThe new file will be derived from the existing language\\n\\tyou have "
"selected in the step before ($SELECTED_LANG). Please make\\n\\tsure the "
"translation you have chosen as base <u>was complete</u>.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:549
msgid "Generate new Language"
msgstr ""

#: antiX-multilanguage-test-suite.sh:552
msgid ""
"\\n\\tPlease press Button “Generate New Language“ in order\\n\\tto manually "
"create a new language for translation not\\n\\tknown by System yet.\\n\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:557
msgid "New Languages available:"
msgstr ""

#: antiX-multilanguage-test-suite.sh:557 antiX-multilanguage-test-suite.sh:604
msgid "Back to Language selection"
msgstr ""

#: antiX-multilanguage-test-suite.sh:604
msgid ""
"\\n<b>Adding manually new Language unknown\\nto system by now.</b>\\n"
"\\tPlease enter the new Language Identifier.\\n\\tExamples for valid "
"Identifiers are:\\n\\n\\t\\t“fr, pt_BR, de@hebrew,\\n\\t\\ten@cyrillic, "
"zh_CN.GB2312”.\\n\\n\\tYou should be absoulutely sure about what\\n\\tyou "
"are doing here before proceeding."
msgstr ""

#: antiX-multilanguage-test-suite.sh:604
msgid "New Language ID"
msgstr ""

#: antiX-multilanguage-test-suite.sh:604
msgid "Create entered ID"
msgstr ""

#: antiX-multilanguage-test-suite.sh:637
msgid ""
"\\n\\tA translation file was found for this language:\\n\\t$LANG_BASE_DIR"
"$SELECTED_LANG$LANG_MSG_DIR$SELECTED_APPLICATION.mo\\n\\n\\tProceed by "
"editing this file?"
msgstr ""

#: antiX-multilanguage-test-suite.sh:640
msgid ""
"\\n\\tProceed using existing language folder?\\n\\n\\t$LANG_BASE_DIR"
"$SELECTED_LANG\\n\\n?"
msgstr ""

#: antiX-multilanguage-test-suite.sh:647
msgid "\\n<b>The language ID you‛ve just entered exists already</b>.$TXT_BODY"
msgstr ""

#: antiX-multilanguage-test-suite.sh:648
msgid "Use existing Language"
msgstr ""

#: antiX-multilanguage-test-suite.sh:676
msgid ""
"Decompiling ˮ.moˮ type “$SELECTED_LANG” language file to editable\\nˮ.poˮ "
"type language file."
msgstr ""

#: antiX-multilanguage-test-suite.sh:722
msgid ""
"Creating empty “.po” type language file prepared for\\n“$SELECTED_LANG” "
"translation."
msgstr ""

#: antiX-multilanguage-test-suite.sh:723
msgid ""
"An empty “.po” type language file has been created, prepared\\nfor "
"“$SELECTED_LANG” translation of “$SELECTED_APPLICATION” program.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:741
msgid ""
"$TXT_DLG_MSG Please save changes when ready\\nbefore closing ˮpoeditˮ, "
"mandatory using filename\\n\\n\\tˮ$FILENAME_POˮ\\n\\nin the working-directory"
"\\n\\n\\tˮ$WORK_DIR_SESSIONˮ.\\n\\nPlease make sure the corresponding ˮ.moˮ "
"file\\nwill get compiled also.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:742
msgid "\\n\\tLanguage: $SELECTED_LANG\\n\\tApplication: $SELECTED_APPLICATION"
msgstr ""

#: antiX-multilanguage-test-suite.sh:745
msgid "Back to Application Selection"
msgstr ""

#: antiX-multilanguage-test-suite.sh:745 antiX-multilanguage-test-suite.sh:1172
msgid "Back to Language Selection"
msgstr ""

#: antiX-multilanguage-test-suite.sh:745
msgid "Start Editor"
msgstr ""

#: antiX-multilanguage-test-suite.sh:852
msgid ""
"\\n<b>ERROR</b>: Your compiled languagefile\\n\\n"
"\\t“$SELECTED_APPLICATION_MO”\\n\\nwas not found in expected folder\\n\\n"
"\\t“$WORK_DIR_SESSION”.\\n\\nDid you save it using correct\\nfolder and "
"filename?\\n\\nCan‛t proceed.\\n\\nIn case you just saved your file to a "
"different\\nfolder, please press “go back to edit“, reopen\\nit in poedit "
"and save it to the correct folder.\\nPlease make sure the “.mo” type of "
"language\\nfile is created by poedit also."
msgstr ""

#: antiX-multilanguage-test-suite.sh:853
msgid "Back to edit"
msgstr ""

#: antiX-multilanguage-test-suite.sh:853
msgid "EXIT"
msgstr ""

#: antiX-multilanguage-test-suite.sh:858
msgid ""
"The file ˮ$SELECTED_APPLICATION.poˮ ($SELECTED_LANG) in this folder has been "
"decompiled from the file ˮ$SELECTED_APPLICATION_MOˮ in original system "
"folder.\\nIt may have been edited with poedit, but was not recompiled to .mo "
"file type before leaving poedit.\\n\\nOriginal Folder: $LANG_BASE_DIR"
"$SELECTED_LANG$LANG_MSG_DIR"
msgstr ""

#: antiX-multilanguage-test-suite.sh:876
msgid ""
"\\nPlease enter the command to start the application corresponding to\\nthe "
"translation you‛ve just edited into the language-file test-tool window"
"\\nbesides. You should observe carefully whether all functions in the program"
"\\ndo work as expected with your translation file, whether all text strings "
"are\\ndisplayed correctly and also whether everything is in its place still. "
"Please\\nclose the tool-window to continue after you are done with checking."
msgstr ""

#: antiX-multilanguage-test-suite.sh:882
msgid "Close"
msgstr ""

#: antiX-multilanguage-test-suite.sh:889
msgid "Test-bench Window ($SELECTED_LANG) - $TXT_TITLE"
msgstr ""

#: antiX-multilanguage-test-suite.sh:924
msgid ""
"\\nDo you want the .mo-file \\n\\n\\tˮ$SELECTED_APPLICATION_MOˮ "
"($SELECTED_LANG)\\n\\nyou‛ve just edited to be copied back to its original "
"place in\\nlanguage system folder structure\\n\\n\\tˮ$FILENAME_MOˮ\\n\\nof "
"the antiX instance just operating this PC?\\n\\nIn any case you‛ll find all "
"resulting files\\nwithin working-directory\\n\\n\\t$WORK_DIR_SESSION\\n"
"\\nafter leaving."
msgstr ""

#: antiX-multilanguage-test-suite.sh:925 antiX-multilanguage-test-suite.sh:1413
#: antiX-multilanguage-test-suite.sh:1446
msgid "Back to Edit"
msgstr ""

#: antiX-multilanguage-test-suite.sh:926
msgid "Yes"
msgstr ""

#: antiX-multilanguage-test-suite.sh:927
msgid "No"
msgstr ""

#: antiX-multilanguage-test-suite.sh:946
msgid ""
"The file “$SELECTED_APPLICATION_MO” in this folder contains the modified "
"“$SELECTED_LANG” language translation of this file.\\nIt has NOT been copied "
"back to its original folder in system.\\n\\nOriginal Folder: $LANG_BASE_DIR"
"$SELECTED_LANG$LANG_MSG_DIR\\n\\nThe .po file is the decompilation of the ."
"mo file and directly accessable for further modification in “poedit”.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:951
msgid ""
"The original file ˮ$SELECTED_APPLICATION_MOˮ ($SELECTED_LANG) has been saved "
"to ˮ$SELECTED_APPLICATION_MO.originalˮ in this folder for backup.\\n\\nThe "
"file “$SELECTED_APPLICATION_MO” in this folder contains the modified "
"“$SELECTED_LANG” language translation of this file.\\n\\nOriginal Folder: "
"$LANG_BASE_DIR$SELECTED_LANG$LANG_MSG_DIR\\n\\nThe modified version has been "
"copied back to original system folder and is in use on the instance of antix "
"running this PC."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1005
msgid "\\n<b>Please select folder from pulldown menu</b>\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1007
msgid "Available Folders:"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1010
msgid "Reset"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1011
msgid "Back to Selection"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1031
msgid ""
"\\n<b>Please select the “.desktop” entry from pulldown menu</b>\\nreferring "
"to the antiX menu entry you want to\\ntranslate or correct.\\n\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1032
msgid "          Selected Directory:    $DESK_ENTRY_DIR\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1033
msgid "Available Menue Entries:"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1036
msgid "Change Directory"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1037
msgid "Proceed"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1114
msgid "<b>Select language</b>"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1115
msgid ""
"\\n       Folder: $DESK_ENTRY_DIR\\n       Desktop-File:  "
"$SELECTED_DESKTOP_ENTRY"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1116
#: antiX-multilanguage-test-suite.sh:1177
msgid "\\nLanguages found in File:\\n$FOUND_LANG"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1117
msgid ""
"\\nSelect a <u>language present</u> in Desktop-file\\nto edit a translation, "
"or <u>any other language</u>\\nto create a new translation."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1118
msgid "Languages:"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1121
#: antiX-multilanguage-test-suite.sh:1173
msgid "Back to Entry Selection"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1174
msgid "Test Edits"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1175
msgid ""
"\\n<b>Add, Delete or Edit Translations of Strings</b>\\nAs long as a "
"language entry is empty, the original (untranslated)\\nor (if present) the "
"unsiversal translation string for this language\\n(two-letter language "
"identifier) will be used instead by antiX\\nsystem. You may remove a "
"language entry by emptying\\nthe corresponding string."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1176
msgid ""
"\\n\\tFolder: $DESK_ENTRY_DIR\\n\\tDesktop-File:  $SELECTED_DESKTOP_ENTRY\\n"
"\\tLanguage:  $SELECTED_LANG"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1178
msgid ""
"\\nOriginal Strings (untranslated):\\n\\t<u>Name:</u>\\t$DESK_NAME\\n"
"\\t<u>Generic Name</u>:\\t$DESK_GENERIC\\n\\t<u>Comment:</u>\\t$DESK_COMMENT"
"\\n\\nTranslations to “$SELECTED_LANG“ Language:"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1252
msgid ""
"The modified “Name” language entry ($SELECTED_LANG) has been replaced in "
"file."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1260
msgid "A new “Name” language entry ($SELECTED_LANG) has been added to file."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1268
msgid ""
"Existing “Name” language entry ($SELECTED_LANG) has been removed from file"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1275
msgid ""
"The new “Name” language entry ($SELECTED_LANG) was identical to entry found "
"in file. Entry has not been touched."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1280
msgid ""
"Existing empty “Name” language entry ($SELECTED_LANG) has been removed from "
"file. Shouldn‛t have been there."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1285
msgid ""
"No “Name” language entry ($SELECTED_LANG) was found in file, no new entry "
"has been written."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1296
msgid ""
"The modified “GenericName” language entry ($SELECTED_LANG) has been replaced "
"in file."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1305
msgid ""
"A new “GenericName” language entry ($SELECTED_LANG) has been added to file."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1314
msgid ""
"Existing “GenericName” language entry ($SELECTED_LANG) has been removed from "
"file"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1321
msgid ""
"The new “GenericName” language entry ($SELECTED_LANG) was identical to entry "
"found in file. Entry has not been touched."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1326
msgid ""
"Existing empty “Generic Name” language entry ($SELECTED_LANG) has been "
"removed from file. Shouldn‛t have been there."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1331
msgid ""
"No “GenericName” language entry ($SELECTED_LANG) was found in file, no new "
"entry has been written."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1342
msgid ""
"The modified “Comment” language entry ($SELECTED_LANG) has been replaced in "
"file."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1351
msgid "A new “Comment” language entry ($SELECTED_LANG) has been added to file."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1360
msgid ""
"Existing “Comment” language entry ($SELECTED_LANG) has been removed from file"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1367
msgid ""
"The new “Comment” language entry ($SELECTED_LANG) was identical to entry "
"found in file. Entry has not been touched."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1372
msgid ""
"Existing empty “Comment” language entry ($SELECTED_LANG) has been removed "
"from file. Shouldn‛t have been there."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1377
msgid ""
"No “Comment” language entry ($SELECTED_LANG) was found in file, no new entry "
"has been written."
msgstr ""

#: antiX-multilanguage-test-suite.sh:1403
msgid ""
"$TXT_TITLE, Version $VERSION\\n\\nThe original file "
"“$SELECTED_DESKTOP_ENTRY” has been saved to “$SELECTED_DESKTOP_ENTRY."
"original” in this folder for backup.\\nThe file “$SELECTED_DESKTOP_ENTRY” in "
"this folder contains following modifications:\\n\\n\\t1.) $TXT_DBG_NAME\\n\\n"
"\\t2.) $TXT_DBG_GENERIC\\n\\n\\t3.) $TXT_DBG_COMMENT\\n\\nOriginal Folder: "
"$DESK_ENTRY_DIR\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1409
msgid ""
"<b>Check edits in menu</b>\\nYou will find your modifications in antiX menu "
"now.\\n<u>While keeping this window open</u>, please check whether they meet "
"the requirements,\\nand decide if you want to keep, dismiss, or re-edit the "
"translation of the menu entry.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1410
msgid ""
"\\nThe modified entry is overlayed to the menue, irrespective of its "
"language. The entry\\nwill be visable off this test suite only when antiX "
"system is localised accordingly.\\nPlease be aware of a <u>possibly new "
"sorting</u> of this entry in menu, depending\\non its new first letter. In "
"any case you will find all files in working directory\\n\\n"
"\\t“$WORK_DIR_SESSION”\\n\\nafter leaving.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1411
msgid "Keep Changes, Exit"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1412
msgid "Restore Original Version, Exit"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1437
msgid ""
"The language modified version of this file was NOT activated on the antiX "
"instance just operating this PC.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1438
msgid ""
"The language modified version of this file is in use on the antiX instance "
"just operating this PC.\\n"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1443
msgid ""
"\\nNo modifications have been made to original translation.\\nDo you want to "
"edit Entry again?"
msgstr ""

#: antiX-multilanguage-test-suite.sh:1444
msgid ""
"\\nIn any case you will find all files in working directory\\n\\t"
"$WORK_DIR_SESSION\\nafter leaving.\\n"
msgstr ""
